#!/usr/bin/env iocsh.bash
###############################################################################
#
# EPICS startup script to launch IOC using module to control a ETALON Multiline2
# Interferometer via its own TCP/IP control server.
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, Lund, 2020
# -----------------------------------------------------------------------------
# WP12 - tamas.kerenyi@ess.eu
# WP12 - douglas.bezerra.beniz@esss.se
#
###############################################################################

# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require(attocubeids3010)

# -----------------------------------------------------------------------------
# ESTIA - enrivonment params
# -----------------------------------------------------------------------------
epicsEnvSet(IPADDR,   "192.168.1.1")
epicsEnvSet(IPPORT,   "9090")
epicsEnvSet(PREFIX,   "ESTIA-ATTOCUBE-001")
epicsEnvSet(PORTNAME, "$(PREFIX)")
epicsEnvSet(LOCATION, "ESTIA; $(IPADDR)")
# Default paths to locate database and protocol
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(attocubeids3010_DIR)db/")
epicsEnvSet(EPICS_DB_INCLUDE_PATH, "$(attocubeids3010_DIR)db/")

# -----------------------------------------------------------------------------
# Specifying the TCP endpoint and port name
# -----------------------------------------------------------------------------
drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

# -----------------------------------------------------------------------------
# Loading databases with EPICS records definition
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

#Load your database defining the EPICS records
dbLoadRecords("attocubeIds3010.db","P=$(PREFIX), PORT=$(PORTNAME), R=:, ADDR=$(IPPORT)")

